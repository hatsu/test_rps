const oldInterval = window.setInterval;
const intervalList = [];
window.setInterval = (fn, delay) => {
  let interval = oldInterval(fn, delay);
  intervalList.push(interval);
  return interval;
};
window.clearAllInterval = () => {
  for (const interval of intervalList) {
    clearInterval(interval);
  }
};
