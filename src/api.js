import "./init.js";

const DELAYS_NORMAL = [1, 2, 1, 5, 40, 1, 3];
const DELAYS_OVERLOADED = [100, 300, 50, 500, 5000, 800, 200];
const RPS_LIMIT = 10;
const IS_OVERLOADED = true;
const CLIENT_STACK_LIMIT = 10;

function* timeoutGenerator(values) {
  let index = 0;
  while (true) {
    index++;
    if (index >= values.length) index = 0;
    yield values[index];
  }
}

function* coordsGenerator(RPSLimit) {
  let index = 0;
  const reqList = [];
  while (true) {
    let newValue = [
      Math.round(Math.random() * 30),
      Math.round(Math.random() * 30)
    ];
    index++;
    reqList.push(Date.now());
    if (reqList.length > RPSLimit) {
      // console.log(reqList[reqList.length - 1] - reqList[0]);
      if (reqList[reqList.length - 1] - reqList[0] <= 1000) {
        const errMsg = "(server) RPS Error: too many requests";
        console.error(errMsg);
        throw new Error(errMsg);
      }
      reqList.shift();
    }
    yield { id: index, coordinates: newValue };
  }
}

const delay = (x) => new Promise((r) => setTimeout(r, x));

const timeout_normal = timeoutGenerator(DELAYS_NORMAL);
const timeout_overloaded = timeoutGenerator(DELAYS_OVERLOADED);

const coords = coordsGenerator(RPS_LIMIT);

export const getNewCoords = async ({ overloaded = false }) => {
  let newTimeout = overloaded
    ? timeout_overloaded.next().value
    : timeout_normal.next().value;
  await delay(newTimeout);
  return coords.next().value;
};

class Fetcher {
  static reqCount = 0;
  static resolveReq() {
    Fetcher.reqCount--;
    if (Fetcher.reqCount < 0) Fetcher.reqCount = 0;
  }

  constructor() {}

  getCoords() {
    Fetcher.reqCount++;
    if (Fetcher.reqCount > CLIENT_STACK_LIMIT) {
      window.clearAllInterval();
      const errMsg = `(client) Request Stack Overflow: Number of Requests is ${Fetcher.reqCount}. Maximum allowed number is 10.`;
      console.error(errMsg);
      throw new Error(errMsg);
    } else {
      let newReq = getNewCoords({ overloaded: IS_OVERLOADED });
      newReq.then(() => Fetcher.resolveReq());
      return newReq;
    }
  }
}

export const getCoords = new Fetcher().getCoords;
