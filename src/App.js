import './App.css';
import React, { useEffect, useState, useRef } from "react";

import { getCoords } from "./api";

function App() {
  const canvas = useRef(null);
  const [currentCoord, setCurrentCoord] = useState([0, 0]);
  useEffect(() => {
    if (canvas.current) {
      const ctx = canvas.current.getContext("2d");
      ctx.fillStyle = "rgb(255, 255, 255, 0.1)";
      ctx.fillRect(0, 0, canvas.current.width, canvas.current.height);
      ctx.fillStyle = "rgb(0, 0, 0,1)";
      ctx.fillRect(currentCoord[0] * 10, currentCoord[1] * 10, 10, 10);
    }
  }, [currentCoord]);

  return (
    <div className="App">
      <canvas ref={canvas} width={300} height={300} />
      <ul>
        <li>Points list</li>
      </ul>
    </div>
  );
}

export default App;
