
# Отрисовка координат, получаемых с сервера.

Отрисовка координат, получаемых с сервера.
Задачи:
  1. Используя метод getCoords получать актуальные координаты точки. (функция имитирует работу endpoint'а)
    Метод возвращает promise. Результат промиса в формате { id: Number, coordinates: [x:Number, y:Number] }  
    Запрашивать данные с максимально возможной скоростью.
    Не превышать лимит в 9-10 запросов в секунду. (rps<10)
    Точки должны быть отрисованы последовательно.
  2. Отобразить 10 последних полученных координат.
  